import 'package:get/get.dart';
import '../models/menu_products.dart';

class CartController extends GetxController {
  var _products = {}.obs;

  void addProduct(Menu_Products product) {
    if (_products.containsKey(product)) {
      _products[product] += 1;
    } else {
      _products[product] = 1;
    }
    Get.snackbar(
      'Product Added',
      'You have add the${product.name} to the cart',
      snackPosition: SnackPosition.TOP,
      duration: Duration(seconds: 1),
    );
  }

  void addProductInCart(Menu_Products product) {
    if (_products.containsKey(product)) {
      _products[product] += 1;
    } else {
      _products[product] = 1;
    }
  }

  void removeProduct(Menu_Products product) {
    if (_products.containsKey(product) && _products[product] == 1) {
      _products.removeWhere((key, value) => key == product);
    } else {
      _products[product] -= 1;
    }
  }

  void removeAllProduct(Menu_Products product) {
    if (_products.containsKey(product)) {
      _products.remove(product);
    } else {
      _products[product] = 0;
    }
  }

  get products => _products;

  get productSubtotal => _products.entries
      .map((product) => product.key.price * product.value)
      .toList();


  get total => _products.entries
      .map((product) => product.key.price * product.value)
      .toList()
      .reduce((value, element) => value + element)
      .toStringAsFixed(0);
}
