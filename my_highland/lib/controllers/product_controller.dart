import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import '../models/menu_products.dart';

List<Menu_Products> parseMenuProduct(String responseBody2){
  var list = json.decode(responseBody2) as List<dynamic>;
  List<Menu_Products>? menu_product = list.map((model) => Menu_Products.fromJson(model)).toList();
  return menu_product;
}

class ProductControllerCoffee extends GetxController {
  var isLoading = true.obs;
  var productList = <Menu_Products>[].obs;

  @override
  void onInit() {
    fetchProductCoffee();
    super.onInit();
  }

  void fetchProductCoffee() async {
    try {
      isLoading(true);
      var products = await RemoteServices.fetchMenuProduct();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}

class ProductControllerCake extends GetxController {
  var isLoading = true.obs;
  var productList = <Menu_Products>[].obs;

  @override
  void onInit() {
    fetchProductCake();
    super.onInit();
  }

  void fetchProductCake() async {
    try {
      isLoading(true);
      var products = await RemoteServices.fetchMenuCake();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}

class ProductControllerTea extends GetxController {
  var isLoading = true.obs;
  var productList = <Menu_Products>[].obs;

  @override
  void onInit() {
    fetchProductTea();
    super.onInit();
  }

  void fetchProductTea() async {
    try {
      isLoading(true);
      var products = await RemoteServices.fetchMenuTea();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}

class ProductControllerPackage extends GetxController {
  var isLoading = true.obs;
  var productList = <Menu_Products>[].obs;

  @override
  void onInit() {
    fetchProductPackage();
    super.onInit();
  }

  void fetchProductPackage() async {
    try {
      isLoading(true);
      var products = await RemoteServices.fetchMenuPackage();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}

class ProductControllerFreeze extends GetxController {
  var isLoading = true.obs;
  var productList = <Menu_Products>[].obs;

  @override
  void onInit() {
    fetchProductFreeze();
    super.onInit();
  }

  void fetchProductFreeze() async {
    try {
      isLoading(true);
      var products = await RemoteServices.fetchMenuFreeze();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}

class RemoteServices {
  static Future<List<Menu_Products>> fetchMenuCake() async{
    String url = 'https://6272406fc455a64564be3960.mockapi.io/api/cake_menu';
    Uri uri = Uri.parse(url);
    final response = await http.get(uri);
    if(response.statusCode == 200)
    {
      return compute(parseMenuProduct, response.body);
    } else {
      throw Exception('Request API Error');
    }
  }

  // ===============================================================

  static Future<List<Menu_Products>> fetchMenuProduct() async{
    String url = 'https://6272406fc455a64564be3960.mockapi.io/api/cafe_menu';
    Uri uri = Uri.parse(url);
    final response = await http.get(uri);
    if(response.statusCode == 200)
    {
      return compute(parseMenuProduct, response.body);
    } else {
      throw Exception('Request API Error');
    }
  }

  // ===============================================================

  static Future<List<Menu_Products>> fetchMenuTea() async{
    String url = 'https://6272406fc455a64564be3960.mockapi.io/api/tea_menu';
    Uri uri = Uri.parse(url);
    final response = await http.get(uri);
    if(response.statusCode == 200)
    {
      return compute(parseMenuProduct, response.body);
    } else {
      throw Exception('Request API Error');
    }
  }

  // ===============================================================

  static Future<List<Menu_Products>> fetchMenuFreeze() async{
    String url = 'https://628e0ae4368687f3e70f500b.mockapi.io/api/freeze';
    Uri uri = Uri.parse(url);
    final response = await http.get(uri);
    if(response.statusCode == 200)
    {
      return compute(parseMenuProduct, response.body);
    } else {
      throw Exception('Request API Error');
    }
  }

  // ===============================================================
  static Future<List<Menu_Products>> fetchMenuPackage() async{
    String url = 'https://628e0ae4368687f3e70f500b.mockapi.io/api/package_products';
    Uri uri = Uri.parse(url);
    final response = await http.get(uri);
    if(response.statusCode == 200)
    {
      return compute(parseMenuProduct, response.body);
    } else {
      throw Exception('Request API Error');
    }
  }
}