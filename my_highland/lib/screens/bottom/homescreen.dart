import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_highland/screens/accounts/invitescreen.dart';
import 'package:my_highland/screens/bottom/orderscreen.dart';
import 'package:my_highland/screens/news.dart';
import 'package:my_highland/widget/carousel.dart';
import 'package:my_highland/widget/menu_products.dart';
import '../../controllers/product_controller.dart';
import '../../models/menu_products.dart';
import '../accounts/Loginscreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ProductControllerCoffee productController = Get.put(ProductControllerCoffee());
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage("https://image.shutterstock.com/image-photo/"
              "white-soft-artificial-fur-abstract-"
              "260nw-1878387613.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          titleTextStyle: TextStyle(
            color: Colors.black,
            fontSize: 14,
          ),
          title: Text('Cùng đón một ngày thú vị nhé!',
            style: TextStyle(fontSize: 15),),
          actions: [
            IconButton(
              onPressed: (){

              },
              icon: Icon(
                Icons.qr_code_scanner,
                color: Colors.black,
              ),
            ),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            Card (
              margin: EdgeInsets.all(10),
              color: Color(0xFFCC3300),
              elevation: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Align(
                    heightFactor: 0.5,
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(Icons.navigate_next, color: Colors.white,),
                      onPressed: (){},
                    ),
                  ),
                  ListTile(
                    title: Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text(
                        "Chào bạn!",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white
                        ),
                      ),
                    ),
                    contentPadding: EdgeInsets.symmetric(horizontal: 25),
                    subtitle: Text(
                      'Tham gia chương trình thành viên của '
                          'Highlands Coffee để tích điểm và '
                          'nhận quà hấp dẫn',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(25),
                    child: Row(
                      children: [
                        FlatButton(
                            child: Text('Đăng ký / Đăng nhập', style: TextStyle(fontSize: 13),),
                            color: Colors.white,
                            textColor: Color(0xFFCC3300),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const LoginScreen()),
                              );
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)
                            )
                        ),
                      ],
                    ),

                  ),
                ],
              ),
            ),
            Material(
              elevation: 0,
                child: Column(
                  children: [
                    SizedBox(
                      height:150,
                      child: Card (
                        color: Color(0xFFFFF3E0),
                        elevation: 0,
                        child: InkWell(
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InviteScreen()
                                ));
                          },
                          child: Row(
                            children: [
                              Expanded(
                                  child: ListTile(
                                    leading: Icon (
                                        Icons.account_box,
                                        color: Color(0xFFCC3300),
                                        size: 60
                                    ),
                                    title: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          heightFactor: 0.4,
                                          alignment: Alignment.topRight,
                                          child: IconButton(
                                            icon: Icon(Icons.navigate_next, color: Colors.grey,),
                                            onPressed: (){},
                                          ),
                                        ),
                                        Text(
                                          "ĐĂNG KÝ TÀI KHOẢN MỚI",
                                          style: TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(height: 10,),
                                        Text(
                                            'TẶNG BẠN MÃ GIẢM GIÁ 50%',
                                            style: TextStyle(fontSize: 15)
                                        ),
                                        Text(
                                            'CHÀO BẠN MỚI',
                                            style: TextStyle(fontSize: 15)
                                        ),
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 200,
                      child: Column(
                        children: [
                          Align(
                            heightFactor: 0.3,
                            alignment: Alignment.topRight,
                            child: TextButton(
                              child: Text('Tất cả',
                              style: TextStyle(
                                color: Color(0xFFFFCCBC),
                                fontWeight: FontWeight.bold,
                              ),),
                              onPressed: (){

                              },
                            ),
                          ),
                          Row(
                            children: [
                              SizedBox(width: 10,),
                              Expanded(
                                  child: Text('Ưu Đãi Đặt Biệt Hôm Nay',
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(child: CarouselDemo()),

                        ],
                      ),
                    ),
                    SizedBox(
                      height: 250,
                      child: Column(
                        children: [
                          SizedBox(height: 10,),
                          Row(
                            children: [
                              SizedBox(width: 10,),
                              Expanded(
                                  child: Text('Sản Phẩm Nổi Bật',
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            child: Obx(() => Container(
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: productController.productList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return MenuProducts(productController.productList[index]);
                                },
                                padding: EdgeInsets.all(10),
                                shrinkWrap: true,
                              ),
                            ),
                          ),),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 100,
                      child: Column(
                        children: [
                          Align(
                            heightFactor: 0.4,
                            alignment: Alignment.topRight,
                            child: Container(
                              color: Color(0xFFCC3300),
                              margin: EdgeInsets.all(10),
                              child: TextButton(
                                child: Text('Menu', style:
                                TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const OrderScreen()),
                                  );
                                },
                                // shape: RoundedRectangleBorder(
                                //     borderRadius: BorderRadius.circular(10.0)
                                // ),
                              ),
                            ),
                          ),
                          Row(
                            children: [
                              SizedBox(width: 10,),
                              Expanded(
                                  child: Text('Khám Phá Toàn Bộ Menu Nào!',
                                    style: TextStyle(
                                        color: Color(0xFFCC3300),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400
                                    ),
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 300,
                      child: Column(
                        children: [
                          Align(
                            heightFactor: 0.3,
                            alignment: Alignment.topRight,
                            child: TextButton(
                              child: Text('Tất cả',
                                style: TextStyle(
                                  color: Color(0xFFFFCCBC),
                                  fontWeight: FontWeight.bold,
                                ),),
                              onPressed: (){

                              },
                            ),
                          ),
                          Row(
                            children: [
                              SizedBox(width: 10,),
                              Expanded(
                                  child: Text('Tin Tức',
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              image: DecorationImage(
                                image: NetworkImage("https://media.foody.vn/res/g26/252481/prof/s/foody-upload-api-foody-mobile-frame-discount-200617093103.jpg"),
                                fit: BoxFit.cover,
                              ),
                            ),
                            margin: EdgeInsets.all(25),
                            child: Row(
                              children: [
                                SizedBox(width: 20,),
                                Column(
                                  children: [
                                    SizedBox(
                                      height: 150,
                                    ),
                                    FlatButton(
                                        child: Text('Xem Thêm', style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500),),
                                        color: Colors.white,
                                        textColor: Color(0xFFCC3300),
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => const NewsSCreen()),
                                          );
                                        },
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(10.0)
                                        )
                                    ),
                                  ],
                                )
                              ],
                            ),

                          ),
                        ],
                      ),
                    ),
                    Center(
                      heightFactor: 3,
                      child: Column(
                        children: [
                          Icon(Icons.coffee),
                          Text('Bản tin đến đây là hết. Xin cám ơn!')
                        ],
                      ),
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}
