import 'package:flutter/cupertino.dart';

class PrePayScreen extends StatefulWidget {
  const PrePayScreen({Key? key}) : super(key: key);

  @override
  State<PrePayScreen> createState() => _PrePayScreenState();
}

class _PrePayScreenState extends State<PrePayScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height/1.5,
            child: Image.network('https://provelife.in/img/cms.png'),
          ),
          Text('Chức năng sẽ được hoàn thiện trong thời gian tới'),
          Text('Xin cám ơn ạ!')
        ],
      ),
    );
  }
}
