import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:http/http.dart';
import 'package:my_highland/component/cart_products.dart';
import 'package:my_highland/screens/bottom/orderscreen.dart';
import 'package:my_highland/screens/bottom/your_order_screen.dart';
import 'package:my_highland/screens/cart/cart_total.dart';

import '../../controllers/cart_controller.dart';
class PaymentScreen extends StatelessWidget {
  final cartController = Get.put(CartController());

  PaymentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        title: Center(
          child: Text(
            'Thanh toán',
            style: TextStyle(
                color: Colors.black
            ),
          ),
        ),
      ),

      body: CartProduct(),

      bottomNavigationBar: Container(
        height: 100,
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
                child: CartTotal()
            ),
            Expanded(
                child: MaterialButton(
                  child: Text(
                    'Mua',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),),
                  onPressed: (){
                    Fluttertoast.showToast(
                        msg: "Buy Successfully",  // message
                        toastLength: Toast.LENGTH_SHORT, // length
                        gravity: ToastGravity.CENTER,    // location
                        timeInSecForIosWeb: 1               // duration
                    );
                        // .then((value) => Navigator.push(
                        // context,
                        // MaterialPageRoute(
                        //     builder: (context) => YourOrderScreen())));
                  },
                  color: Colors.redAccent,
                )
            ),
          ],
        ),
      ),
    );
  }
}
