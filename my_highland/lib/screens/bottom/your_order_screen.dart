import 'package:flutter/material.dart';
import 'package:my_highland/ui/app.dart';
import '../../controllers/cart_controller.dart';
import '../../models/menu_products.dart';

class YourOrderScreen extends StatelessWidget {
  const YourOrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final CartController controller = Get.find();
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Đơn hàng',
            style: TextStyle(
                color: Colors.black,
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.black,),
          onPressed: (){
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MyStatefulWidget()));
          },
        ),
      ),
      // body: Obx(() => SizedBox(
      //   height: 700,
      //   child: ListView.builder(
      //       shrinkWrap: true,
      //       physics: ScrollPhysics(),
      //       itemCount: controller.products.length,
      //       itemBuilder: (BuildContext context, int index){
      //         return CartProductCard2(
      //           controller: controller,
      //           quantity: controller.products.values.toList()[index],
      //           product: controller.products.keys.toList()[index],
      //           index: index,
      //         );
      //       }),
      // ),)
    );
  }
}

class CartProductCard2 extends StatelessWidget {
  final CartController controller;
  final Menu_Products product;
  final int quantity;
  final int index;
  const CartProductCard2({
    Key? key,
    required this.product,
    required this.quantity,
    required this.index,
    required this.controller,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child:  Card(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: 80,
                width: 80,
                child: Column(
                  children: [
                    Expanded(
                      child:Image.network(
                        product.image,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      child: Center(
                        child: Text(
                          '${product.price}\ .đ',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.redAccent,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 60,),
              Expanded(
                  child: Text(
                    product.name,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.deepOrangeAccent
                    ),)
              ),
            ],
          ),
        )
    );
  }
}

