import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_highland/controllers/product_controller.dart';
import 'package:my_highland/screens/TabBarViewScreen/cakescreen.dart';
import 'package:my_highland/screens/TabBarViewScreen/freezescreen.dart';
import 'package:my_highland/screens/TabBarViewScreen/packageproducts.dart';
import 'package:my_highland/screens/TabBarViewScreen/teascreen.dart';
import 'package:my_highland/screens/bottom/payment_screen.dart';

import '../TabBarViewScreen/coffeescreen.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  late final ProductControllerCake productControllerCake;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
          length: 5,
          child: Row(
            children: [
              Expanded(
                  child: Scaffold(
                    appBar: AppBar(
                      actions: [
                        IconButton(onPressed: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => PaymentScreen())
                          );
                        }, icon: Icon(Icons.shopping_cart_outlined))
                      ],
                      leading: IconButton(
                        icon: Icon(Icons.location_on_outlined),
                        onPressed: () {

                        },
                      ),
                      title: Text("Nhập món tại"),
                      backgroundColor: Colors.red,
                      bottom: TabBar(
                        labelColor: Colors.white,
                        tabs: [
                          Tab (child: Text("Cà Phê")),
                          Tab (child: Text("Trà")),
                          Tab (child: Text("Freeze")),
                          Tab (child: Text("Bánh Ngọt")),
                          Tab (child: Text("Sản Phẩm Đóng Gói")),
                        ],
                        isScrollable: true,
                        indicatorColor: Colors.black,
                      ),
                    ),
                    body: TabBarView(
                      children:[
                        CoffeeScreen(),
                        TeaScreen(),
                        FreezeScreen(),
                        CakeScreen(),
                        PackageProduct(),
                      ],
                    ),
                  ))
                ],
              )
      );
  }
}

