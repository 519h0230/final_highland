
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../controllers/cart_controller.dart';
import '../../models/menu_products.dart';
class CartProducts extends StatelessWidget {
  final CartController controller = Get.find();
  CartProducts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => SizedBox(
      height: 700,
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: controller.products.length,
          itemBuilder: (BuildContext context, int index){
            return CartProductCard(
              controller: controller,
              quantity: controller.products.values.toList()[index],
              product: controller.products.keys.toList()[index],
              index: index,
            );
          }),
    ),
    );
  }
}



class CartProductCard extends StatelessWidget {
  final CartController controller;
  final Menu_Products product;
  final int quantity;
  final int index;
  const CartProductCard({
    Key? key,
    required this.product,
    required this.quantity,
    required this.index,
    required this.controller,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child:  Card(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: 80,
                width: 80,
                child: Column(
                  children: [
                    Expanded(
                      child:Image.network(
                        product.image,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      child: Center(
                        child: Text(
                          '${product.price}\ .đ',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.redAccent,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Text(
                    product.name,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.deepOrangeAccent
                    ),)
              ),
              SizedBox(width: 100,),
              Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: (){
                          controller.removeProduct(product);
                        },
                        icon: Icon(Icons.remove_circle),
                      ),
                      Text('${quantity}'),
                      IconButton(
                          onPressed: (){
                            controller.addProductInCart(product);
                          },
                          icon: Icon(Icons.add_circle)
                      ),
                    ],
                  ),
                  TextButton(
                      onPressed: (){
                        controller.removeAllProduct(product);
                        },
                      child: Text('Xóa tất cả',
                        style: TextStyle(
                            color: Color(0xFFFFCCBC),
                            fontWeight: FontWeight.w900
                        ),)),
                ],
              ),

            ],
          ),
        )
    );
  }
}

