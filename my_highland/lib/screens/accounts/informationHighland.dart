import 'package:flutter/material.dart';
const List <String>TextList = <String>[
  'Từ tình yêu với Việt Nam và niềm đam mê cà phê, năm 1999, thương hiệu Highlands Coffee® ra đời với khát vọng nâng tầm di sản cà phê lâu đời của Việt Nam và lan rộng tinh thần tự hào, kết nối hài hoà giữa truyền thống với hiện đại.',
  'Bắt đầu với sản phẩm cà phê đóng gói tại Hà Nội vào năm 2000, chúng tôi đã nhanh chóng phát triển và mở rộng thành thương hiệu quán cà phê nổi tiếng và không ngừng mở rộng hoạt động trong và ngoài nước từ năm 2002.',
  'Qua một chặng đường dài, chúng tôi đã không ngừng mang đến những sản phẩm cà phê thơm ngon, sánh đượm trong không gian thoải mái và lịch sự. Những ly cà phê của chúng tôi không chỉ đơn thuần là thức uống quen thuộc mà còn mang trên mình một sứ mệnh văn hóa phản ánh một phần nếp sống hiện đại của người Việt Nam',
  'Đến nay, Highlands Coffee® vẫn duy trì khâu phân loại cà phê bằng tay để chọn ra từng hạt cà phê chất lượng nhất, rang mới mỗi ngày và phục vụ quý khách với nụ cười rạng rỡ trên môi. Bí quyết thành công của chúng tôi là đây: không gian quán tuyệt vời, sản phẩm tuyệt hảo và dịch vụ chu đáo với mức giá phù hợp.'
];


class InformatioHighland extends StatelessWidget {
  const InformatioHighland({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 60, bottom: 20),
            height: 200,
            decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage('https://www.highlandscoffee.com.vn/vnt_upload/about/ABOUT-ORIGIN.png'),
                )
            ),
            child: Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white,),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Text('Thương hiệu bắt nguồn từ cà phê Việt Nam',
            style: TextStyle(
              fontSize: 20,
                color: Colors.brown,
              fontWeight: FontWeight.w500
            ),
            textAlign: TextAlign.left
            ,),
          Expanded(
            child: ListView.builder(
                itemCount: TextList.length,
                itemBuilder: (BuildContext context, int index){
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Material(
                        child: Text(TextList[index],
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.brown
                          ),
                        ),
                      )
                    ],
                  );
                }
            ),
          )
        ],
      ),
    );
  }
}
