import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_highland/screens/accounts/registrationscreen.dart';
import 'package:my_highland/ui/app.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  bool loading = false;
  bool isLogedin = false;
  late SharedPreferences preferences;

  //from key
  final _formKey = GlobalKey<FormState>();

  //editing controller

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();


  final _auth = FirebaseAuth.instance;


  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async{
    setState((){
      loading = true;
    });
    preferences = await SharedPreferences.getInstance();
    if(isLogedin){
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => MyStatefulWidget()
          )
      );
    }
    setState((){
      loading = false;

    });
  }
  Future<void> main() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var status = prefs.getBool('isLoggedIn') ?? false;
    print(status);
    runApp(MaterialApp(home: status == true ? LoginScreen() : MyStatefulWidget()));
  }

  @override
  Widget build(BuildContext context) {

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,

      validator: (value) {
        if(value!.isEmpty){
          return ("please Enter Your Email");
        }
        // req expression for email validation
        if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]"
        ).hasMatch(value)){
          return ("Please Enter a valid email");
        }
          return null;
      },
      onSaved: (value) {
        emailController.text = value!;
        },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.email),
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        )
      ),
    );

    //passsword field
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,

      //validator: () {},
      validator: (value) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return("Password is required for login");
        }
        if(!regex.hasMatch(value))
          {
            return("Please Enter valid password(Min.6 password)");
          }
      },

      onSaved: (value) {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.password),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          )
      ),
    );

    //Button
    final loginButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(300),
      color: Colors.red,
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: (){
          signIn(emailController.text, passwordController.text);
        },
        child: Text(
          'Login',
          textAlign: TextAlign.center,
        style: TextStyle
          (
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.bold
        ),),
      ),
    );



    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 200,
                      child: Image.asset(
                        "assets/Logo-HighLands-Coffee.png",
                      fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(height: 45,),
                    emailField,
                    SizedBox(height: 15,),
                    passwordField,
                    SizedBox(height: 45,),
                    loginButton,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account?"),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                  MaterialPageRoute(
                                builder: (context) => RegistrationScreen()));
                          },
                          child: Text("SignUp",
                          style: TextStyle(
                            color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void signIn(String email, String password) async
  {
    if(_formKey.currentState!.validate())
      {
        await _auth
            .signInWithEmailAndPassword(email: email, password: password)
            .then((uid) => {
              Fluttertoast.showToast(msg: "Login Successful"),
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => MyStatefulWidget()))
        }).catchError((e) {
          Fluttertoast.showToast(msg: e!.message);
        });
      }
  }
}
