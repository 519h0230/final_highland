import 'package:flutter/material.dart';

class NewsSCreen extends StatelessWidget {
  const NewsSCreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 250,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    "https://media.foody.vn/res/g26/252481/prof/s/foody-upload-api-foody-mobile-frame-discount-200617093103.jpg"
                ),
                fit: BoxFit.cover,
              ),
            ),
            child: Align(
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_outlined,
                  color: Colors.white70,),
                onPressed: () {
                  Navigator.pop(context);
                },),
              alignment: Alignment.topLeft,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.only(left: 20, top: 20, right: 20),
            height: 150,
            width: 10,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('ƯU ĐÃI 50% CHÀO MỪNG THÀNH VIÊN MỚI', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),),
                Text('01 tháng 10, 2021, 16:00'),
                Text('HighLands Coffee tặng ngay mã ưu đãi 50% dành cho Thành Viên Mới',),
                Text('Kiểm tra tại mục Ưu đãi và order ngay thôi!'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
