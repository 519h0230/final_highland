import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:my_highland/widget/menu_freeze.dart';

import '../../controllers/product_controller.dart';

class FreezeScreen extends StatefulWidget {
  const FreezeScreen({Key? key}) : super(key: key);

  @override
  State<FreezeScreen> createState() => _FreezeScreenState();
}

class _FreezeScreenState extends State<FreezeScreen> {
  final ProductControllerFreeze productController = Get.put(ProductControllerFreeze());

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Expanded(
        child: Obx(() => ListView(
          children: [
            GridView.builder(
              physics: ScrollPhysics(),
              itemCount: productController.productList.length,
              itemBuilder: (BuildContext context, int index) {
                return MenuFreeze(productController.productList[index]);
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 5,
              ),
              padding: EdgeInsets.all(10),
              shrinkWrap: true,
            ),
          ],
        ))),
      ],);
  }
}