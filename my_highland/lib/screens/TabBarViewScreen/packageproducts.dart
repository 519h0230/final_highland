import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:my_highland/widget/menu_package_product.dart';

import '../../controllers/product_controller.dart';

class PackageProduct extends StatefulWidget {
  const PackageProduct({Key? key}) : super(key: key);

  @override
  State<PackageProduct> createState() => _PackageProductState();
}

class _PackageProductState extends State<PackageProduct> {
  final ProductControllerPackage productController = Get.put(ProductControllerPackage());
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Obx(() => ListView(
              children: [
                GridView.builder(
                  physics: ScrollPhysics(),
                  itemCount: productController.productList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return MenuPackageProduct(productController.productList[index]);
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 5,
                  ),
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                ),
              ],
            ))),
      ],);
  }
}
