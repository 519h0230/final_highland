import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:my_highland/widget/menu_cake.dart';
import '/controllers/product_controller.dart';
class CakeScreen extends StatefulWidget {
  const CakeScreen({Key? key}) : super(key: key);

  @override
  State<CakeScreen> createState() => _CakeScreenState();
}

class _CakeScreenState extends State<CakeScreen> {
  final ProductControllerCake productController = Get.put(ProductControllerCake());

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Expanded(
            child: Obx(() => ListView(
              children: [
                GridView.builder(
                  physics: ScrollPhysics(),
                  itemCount: productController.productList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return MenuCake(productController.productList[index]);
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 5,
                  ),
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                ),
              ],
            ))),
      ],
    );
  }
}
