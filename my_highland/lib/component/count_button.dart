import 'package:flutter/material.dart';
class CountButton extends StatefulWidget {
  const CountButton({Key? key}) : super(key: key);

  @override
  State<CountButton> createState() => _CountButtonState();
}

class _CountButtonState extends State<CountButton> {
  int _n = 0;

  void add() {
    setState(() {
      _n++;
    });
  }

  void minus() {
    setState(() {
      if (_n != 0)
        _n--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                onPressed: minus,
                child: Icon(
                    Icons.exposure_minus_1,
                    color: Colors.black,
                ),
                backgroundColor: Colors.white,
              ),
              Text('$_n',
                  style: TextStyle(fontSize: 30.0)),
              FloatingActionButton(
                heroTag: null,
                onPressed: add,
                child: Icon(
                  Icons.add, color: Colors.black,
                ),
                backgroundColor: Colors.white,
              ),
            ],
          ),
        ),
    );
  }
}
