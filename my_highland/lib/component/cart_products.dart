import 'package:flutter/material.dart';
import 'package:my_highland/screens/cart/cart_total.dart';

import '../screens/cart/cart_products.dart';
class CartProduct extends StatefulWidget {
  const CartProduct({Key? key}) : super(key: key);

  @override
  State<CartProduct> createState() => _CartProductState();
}

class _CartProductState extends State<CartProduct> {


  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CartProducts(),
      ],
    );
  }
}


