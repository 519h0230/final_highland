import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

final List<String> imgList = [
  'https://mfvietnam.com/wp-content/uploads/2019/05/untitled-3.jpg',
  'https://cdn.tgdd.vn/Files/2021/12/10/1403590/tong-hop-cac-san-pham-cua-highland-coffee-dang-kinh-doanh-202112101133431109.jpg',
  'https://mfvietnam.com/wp-content/uploads/2019/05/highland-app.jpg',
  'https://media.foody.vn/res/g24/230066/prof/s/image-a32c1a3c-210613161457.jpeg',
  'http://img.websosanh.vn/v2/users/review/images/danh-gia-banh-trung-thu/dumpgozuxussi.jpg'
];

final themeMode = ValueNotifier(2);

class CarouselDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      builder: (context, value, g) {
        return Material(
          child: NoonLoopingDemo(),
        );
      },
      valueListenable: themeMode,
    );
  }
}
class NoonLoopingDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: CarouselSlider(
          options: CarouselOptions(
            aspectRatio: 2.0,
            enlargeCenterPage: true,
            enableInfiniteScroll: false,
            initialPage: 2,
            autoPlay: true,
          ),
          items: imageSliders,
        ));
  }
}

final List<Widget> imageSliders = imgList.map((item) => Container(
  child: Container(
    child: ClipRRect(
        borderRadius: BorderRadius.all(
            Radius.circular(5.0)
        ),
        child: Stack(
          children: <Widget>[
            Image.network(item, fit: BoxFit.cover),
            Positioned(
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(100, 0, 0, 0),
                      Color.fromARGB(100, 0, 0, 0)
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
                padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 20.0
                ),
              ),
            ),
          ],
        )),
  ),
)).toList();