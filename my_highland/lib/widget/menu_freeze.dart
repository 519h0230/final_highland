import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../controllers/cart_controller.dart';
import '../controllers/product_controller.dart';
import '../models/menu_products.dart';
import 'menu_products.dart';

class MenuFreeze extends StatelessWidget {
  final cartController = Get.put(CartController());
  final Menu_Products product;
  final ProductControllerFreeze productController = Get.put(ProductControllerFreeze());
  MenuFreeze(this.product);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Color(0xFFFFF3E0),
      padding: EdgeInsets.only(right: 5, left: 5),
      child: Card(
        elevation:5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: MaterialButton(
            onPressed: (){
              Navigator.push(context, MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Material(
                    color: Color(0xFFFFF3E0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(top: 50),
                            height: 250,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(product.image),
                                fit: BoxFit.cover,
                              ),
                            ),
                            child: Align(
                              child: IconButton(
                                icon: Icon(
                                  Icons.arrow_back_ios_outlined,
                                  color: Colors.black,),
                                onPressed: () {
                                  Navigator.pop(context);
                                },),
                              alignment: Alignment.topLeft,
                            ),
                          ),),
                        Container(
                          color: Colors.white,
                          child: Column(
                            children: [
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(padding: EdgeInsets.only(left: 8), child: Title(color: Colors.black,
                                    child: Text(product.name,
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.italic
                                      ),),

                                  ),),
                                  Padding(padding: EdgeInsets.only(right: 8),child: Title(color: Colors.black,
                                    child: Row(
                                      children: [
                                        Text('${product.price}',
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600,

                                          ),),
                                        Text('đ',style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,

                                        ),)
                                      ],
                                    ),

                                  ),),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8, top: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Text('Gía bán đã bao gồm 8% VAT, áp dụng từ ngày'),
                                    Text('01/02/2022 đến ngày 31/12/2022'),
                                  ],
                                ),),
                              SizedBox(height: 10,),
                              Center(
                                child: Row(
                                  children: [
                                    RectButtonSelect(label: 'S'),
                                    RectButton(label: 'M'),
                                    RectButton(label: 'L'),
                                  ],
                                ),
                              ),
                              SizedBox(height: 40,),
                            ],
                          ),
                        ),
                        Material(
                          child: Column(
                            children: [
                              Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: RaisedButton(
                                      onPressed: () {cartController.addProduct(product);},
                                      color: Colors.redAccent,
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(
                                              Icons.card_travel,
                                              color: Colors.white,
                                            ),
                                            SizedBox(
                                              width: 4.0,
                                            ),
                                            Text("Mua",
                                              style: TextStyle(color: Colors.white),
                                              ),
                                          ],
                                        ),
                                      ),),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width/4,
                  child: Center(
                    child: Image.network(product.image,fit: BoxFit.fill,),
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height/60,
                    child: Column(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: Text(
                                product.name,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 13,
                                ),
                              ),
                              subtitle: Row(
                                children: [
                                  Text('${product.price}'),
                                  Text('đ'),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
              ],
            )
        ),
      ),
    );
  }
}