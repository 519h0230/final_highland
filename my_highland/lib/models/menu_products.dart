import 'dart:convert';

List<Menu_Products> productFromJson(String str) =>
    List<Menu_Products>.from(json.decode(str).map((x) => Menu_Products.fromJson(x)));

String productToJson(List<Menu_Products> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Menu_Products {
  String name;
  String image;
  int price;
  String id;

  Menu_Products({
    required this.name,
    required this.image,
    required this.price,
    required this.id
  });

  factory Menu_Products.fromJson(Map<String, dynamic> json) => Menu_Products(
    name : json['name'],
    image : json['image'],
    price : json['price'],
    id : json['id']
  );

  Map<String, dynamic> toJson() => {
    'name' : this.name,
    'image' : this.image,
    'price' : this.price,
    'id' : this.id,
  };
}
